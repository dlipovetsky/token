package main

import (
	"fmt"
	"os"

	"bitbucket.org/dlipovetsky/token/keystone"

	"github.com/gophercloud/gophercloud/openstack"
)

func main() {
	options, err := openstack.AuthOptionsFromEnv()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not build OpenStack auth options from environment: %s\n", err)
		return
	}
	token, err := keystone.Authenticate(options)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not obtain token: %s\n", err)
		return
	}
	fmt.Printf("token: %s\nexpiresAt: %s\n", token.ID, token.ExpiresAt)
}
